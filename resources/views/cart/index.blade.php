@extends('layouts.app')

<?php $items = session()->get('cart'); ?>

@section('content')
<?php if (session('cart') && count($items) >= 1) : ?>
<?php $total = 0; ?>
<h1 class="text-center">Carrito de compra</h1>
<div class="list-group mt-5 mb-3">
    @foreach ($items as $item)
        <?php $product = $item['product']; ?>
        <div class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="row">
                <div class="col-md-3">
                    <img src="{{ route('product.image', ["filename" => $product->image]) }}" class="img_cart" alt="">
                </div>
                <div class="col-md-9">
                    <div class="d-flex w-100 justify-content-between">
                        <h5>{{ $product->name }}</h5>
                        <small>
                            <a href="{{ route('cart.delete', ["id" => $product->id]) }}">
                                <i class="btn btn-danger mt-md-0 mb-5 fa fa-close-personalized fa-close"></i>
                            </a>
                        </small>
                    </div>
                    @if ($product->ship_cost > 0)
                        <p class="mb-1">${{ $product->price }} + ${{$product->ship_cost}} envio</p>
                    @else
                        <p class="mb-1">${{ $product->price }} - ENVIO GRATIS</p>
                    @endif
                    @if ($item['qty'] == 1)
                        <span>{{ $item['qty'] }} unidad</span>
                    @else
                        <span>{{ $item['qty'] }} unidades</span>
                    @endif
                </div>
            </div>
        </div>
        <?php $total += $item['totalPrice']; ?>
    @endforeach
    <div class="mt-5">
        <h2 class="float-md-left">Total: $<?= $total ?></h2>
    </div>
</div>

<div class="cart-options">
    <hr>
    <div class="row">
        <div class="col-md-8 col-7">
            <a class="float-md-left btn btn-lg btn-success" href="">Hacer pedido</a>
        </div>
        <div class="col-md-4 col-5">
            <a href="{{ route('cart.delete') }}" class="btn btn-danger float-md-right">Vaciar carrito</a>
        </div>
    </div>
</div>
<?php else : ?>
    <p>El carrito está vacio, añade productos.</p>
<?php endif; ?>
@endsection