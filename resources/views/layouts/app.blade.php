<?php 
use App\Category; 

$cart_num_items = session()->get('cart') ? count(session()->get('cart')) : 0;
?>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'MiMercado') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg-skyblue shadow-sm">
            <div class="row" style="width: 100%;">
                <div class="col-md-2">
                    <a class="navbar-brand mt-1" href="{{ url('/') }}">
                        <h1>MiMercado</h1>
                    </a>
                </div>
                <div class="col-md-7 d-flex ml-md-5">
                    <input class="mt-md-3 ml-md-5 form-control w-md-75" type="search" placeholder="Buscá algo que te guste..." name="search">
                    <button type="submit" class="btn btn-primary mt-md-3 p-1 w-25 ml-1">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
                <div class="col-md-2 ml-5 mb-md-0">
                    <a href="{{ route('cart.index') }}">    
                        <span class='badge badge-warning float-right mt-md-2' id='lblCartCount'>{{ $cart_num_items  }}</span>
                        <i class="text-light fa fa-shopping-cart float-right mt-md-3 mr-md-3 cart-home cart-size"></i>
                    </a>
                </div>
            </div>
        </nav>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <!--<a href="" class="float-left ml-md-5">
                Categorías
            </a>-->   
            <a href="#" class="float-left ml-md-5 h5 mt-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Categorías
            </a>
            <div class="dropdown-menu ml-md-5" aria-labelledby="dropdownMenuButton">
                <?php $categories = Category::all(); ?>
                @foreach ($categories as $category)
                    <a class="dropdown-item" href="{{ route("category.products", ["id" => $category->id]) }}">{{ $category->name }}</a>
                @endforeach
            </div>         
            @guest
                <a href="{{ route("login") }}" class="float-right ml-md-auto h5">
                    Ingresar
                </a>            
                <a href="{{ route("register") }}" class="float-right ml-md-3 h5">
                    Crear cuenta
                </a>
            @else
                <a href="{{ route("home") }}" class="float-right ml-auto mr-md-3 h5">
                    Mi cuenta
                </a>
                <a class="float-right ml-md-3 mr-md-2 h5" href="{{ route('logout') }}" onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                 {{ __('Salir') }}
                </a>

             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                 @csrf
             </form>
            @endguest
        </nav>
        <div class="container">
            @yield('content')
        </div>
    </div>
    <footer class="bg-skyblue text-light p-3 text-center mt-3">
        <p class="mt-3">MiMercado &copy <?php echo date('Y'); ?> - Todos los derechos reservados</p>
    </footer>
</body>
</html>
