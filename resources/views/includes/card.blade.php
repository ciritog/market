<div class="card-deck mt-5">
    @foreach ($products as $product)
        <div class="card">
            <img class="card-img-top"
                src="{{ route("product.image", ["filename" => $product->image]) }}"
                alt="Card image cap">
            <div class="card-body">
                <a href="{{ route("category.products", ["id" => $product->category->id]) }}">
                    <span class="badge bg-skyblue text-light product-category-font-size">{{ $product->category->name }}</span>
                </a>
                <h4 class="card-title mt-3">{{ $product->name }}</h4>
                <span class="h5">${{ $product->price }}</span>
            </div>
            <div class="card-footer d-flex justify-content-center">
                <a href="{{ route("product.detail", ["id" => $product->id]) }}">
                    <button class="btn btn-primary mr-3">Ver producto</button>
                </a>
                <a href="{{ route('cart.add', ["id" => $product->id]) }}">
                    <button class="btn btn-danger">
                        <i class="text-light fa fa-shopping-cart cart-size"></i>
                    </button>
                </a>
            </div>
        </div>
    @endforeach
</div>