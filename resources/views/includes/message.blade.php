@if (session('message'))
    <div class="alert alert-success text-center alert-dismissible fade show message-text-size" role="alert">
        <i class="d-inline fa fa-check-circle message-icon-size mr-3" aria-hidden="true"></i>
        <span class="d-inline">{{ session('message') }}</span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif