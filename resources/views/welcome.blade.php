@extends('layouts.app')
@section('content')
<div class="latest-products mt-5 p-2">
    <h1 class="text-light ml-3 mt-1">¡¡Nuevos productos!!</h1>
</div>
@include('includes.card', $products)
<div class="row">
    @foreach ($categories as $category)
    <div class="col-md-4 col-12">
        <section class="pt-5 pb-5 mt-5">
            <div class="category">
                <h2 class="text-light text-center ml-3 mt-1 p-2 smaller-font-size">{{ $category->name }}</h2>
            </div>
            <div class="container mt-3">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="list-unstyled">
                            @if (count($category->products) >= 1)
                                @foreach ($category->products as $product)
                                    <li class="row mb-4">
                                        <a href="#" class="col-3">
                                            <img src="{{ route("product.image", ["filename" => $product->image]) }}" alt="{{ $product->name }}"
                                                class="rounded img-fluid">
                                        </a>
                                        <div class="col-9">
                                            <a href="{{ route("product.detail", ["id" => $product->id]) }}">
                                                <h6 class="mb-3 h5 text-dark">{{ $product->name }}</h6>                                          </h6>
                                            </a>
                                            <div class="d-flex text-small">
                                                <h5>${{ $product->price }}</h5>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            @else
                                <p class="text-center h5">No hay productos en esta categoría.</p>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @endforeach
</div>
<div class="more-products text-light text-center p-3">
    <h1>¿Buscás más?</h1>
    <p class="more-products-text">¡Tenemos muchos más productos para ofrecerte!</p>
    <button type="button" class="btn btn-lg btn-outline-light">Ver más</button>
</div>
@endsection
