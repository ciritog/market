@extends('layouts.app')

@section('content')
    <div class="latest-products mt-5 p-2">
        <h2 class="text-light ml-3 mt-1">{{ $category_name }}</h2>
    </div>
    @if (count($products) >= 1)
        @include('includes.card', $products)
    @else
        <p class="mt-5 h5 text-center">No se encontraron productos en esta categoría.</p>
    @endif
@endsection