@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 mt-3">
                <h1 class="text-center">Editar categoría</h1>
                <p class="text-center">Actualizá la información de la categoría.</p>
                @include('includes.message')
                <form method="POST" action="{{ route("category.update") }}" class="mt-5">
                    @csrf
    
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Categoría') }}</label>
    
                        <div class="col-md-6">
                            <input id="category_id" type="hidden" name="id" value="{{ $category->id }}">
                            <input id="name" type="text" placeholder='Ej: Hogar y Electrodomésticos"' class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $category->name }}">
                            @include('includes.categorymessage')
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
    
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Guardar cambios') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection