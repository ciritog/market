@extends('layouts.app')

<?php 
    use App\Category;
    $categories = Category::orderBy("id", "desc")->paginate(5); 
?>

@section('content')
    <h1 class="mt-5">Categorías</h1>
    <p class="h5">Estas son las categorías de tu tienda.</p>
    <a href="{{ route('category.add') }}">
        <button class="btn bg-skyblue text-light mt-3">
            <strong>Agregar categoría</strong>
        </button>
    </a>
    @if(count($categories) >= 1)
        @include('includes.message')
        <table class="table mt-4">
            <thead class="bg-skyblue text-center text-light">
                <tr class="h5">
                    <th scope="col">ID</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Opciones</th>
                </tr>
            </thead>
            <tbody class="text-center">
                @foreach($categories as $category)
                    <tr class="h5">
                        <th scope="row">{{ $category->id }}</th>
                        <td>{{ $category->name }}</td>
                        <td>
                            <a
                                href="{{ route("category.products", ["id" => $category->id]) }}">
                                <button class="btn btn-primary options-font-size">
                                    Ver
                                </button>
                            </a>
                            <a
                                href="{{ route("category.edit", ["id" => $category->id]) }}">
                                <button class="btn btn-primary options-font-size">
                                    <i class="fa fa-pencil"></i>
                                </button>
                            </a>
                            <button type="button" class="btn btn-danger options-font-size" data-toggle="modal"
                                data-target="#exampleModalCenter">
                                <i class="fa fa-trash"></i>
                            </button>
                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">¿Estás seguro/a que deseas
                                                eliminarlo?</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body mt-3">
                                            <p>Al borrar una categoría, <strong>todos los productos que pertenezcan a la
                                                    misma serán
                                                    eliminados</strong>.</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Cancelar</button>
                                            <a href="{{ route("category.delete", ["id" => $category->id]) }}"
                                                class="btn btn-danger">Si, eliminar.</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <!-- PAGINATION -->
        <div class="clearfix mt-5"></div>
        <div class="d-flex justify-content-center">
            {{ $categories->links() }}
        </div>
    @else
        <p class="mt-5 h5 text-center">No se encontraron categorías.</p 
    @endif 
@endsection