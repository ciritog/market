@extends('layouts.app')

@section('content')
    <h1 class="mt-5">Mis productos</h1>
    <p class="h5">Estos son todos los productos de tu tienda.</p>
    <a href="{{ route('product.add') }}">
        <button class="btn bg-skyblue text-light mt-3">
            <strong>Agregar producto</strong>
        </button>
    </a>
    @if(count($products) >= 1)
        @include('includes.message')
        <table class="table mt-4">
            <thead class="bg-skyblue text-center text-light">
                <tr class="h5">
                    <th scope="col-2" style="width: 30%;">Nombre</th>
                    <th scope="col-2">Precio</th>
                    <th scope="col-2">Stock</th>
                    <th scope="col-2">Costo de envio</th>
                    <th scope="col-2">Categoría</th>
                    <th scope="col-2">Opciones</th>
                </tr>
            </thead>
            <tbody class="text-center">
                @foreach($products as $product)
                    <tr class="h5">
                        <td>{{ $product->name }}</td>
                        <td>${{ $product->price }}</td>
                        <td>{{ $product->stock }}</td>
                        <td>${{ $product->ship_cost }}</td>
                        <td>{{ $product->category->name }}</td>
                        <td>
                            <a
                                href="{{ route("product.detail", ["id" => $product->id]) }}">
                                <button class="btn btn-primary options-font-size">Ver</button>
                            </a>
                            <a
                                href="{{ route("product.edit", ["id" => $product->id]) }}">
                                <button class="btn btn-primary options-font-size">
                                    <i class="fa fa-pencil"></i>
                                </button>
                            </a>
                            <button type="button" class="btn btn-danger options-font-size" data-toggle="modal"
                                data-target="#exampleModalCenter">
                                <i class="fa fa-trash"></i>
                            </button>
                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">¿Estás seguro que deseas
                                                eliminarlo?</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body mt-3">
                                            <p>El producto no podrá volver a recuperarse.</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Cancelar</button>
                                            <a href="{{ route('product.delete', ["id" => $product->id]) }}"
                                                class="btn btn-danger">Si, eliminar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <!-- PAGINATION -->
        <div class="clearfix mt-5"></div>
        <div class="d-flex justify-content-center">
            {{ $products->links() }}
        </div>
    @else
        <p class="mt-5 h5 text-center">No se encontraron productos.</p>
    @endif
@endsection