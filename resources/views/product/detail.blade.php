@extends('layouts.app')

@section('content')
    <div class="mt-3">
        <a href="">Volver</a>
    </div>
    <div class="row mt-2">
        <div class="col-md-6">
            <img class="w-100" src="{{ route("product.image", ["filename" => $product->image]) }}" alt="">
        </div>
        <div class="col-md-6">
            <h2 class="mt-md-0 mt-3">{{ $product->name }}</h2>
            <div class="mt-5">
                <h2 class="">${{ $product->price }}</h2>
            </div>
            <div class="ml-1">
                @if ($product->ship_cost > 0)
                    <span>Costo de envio: ${{ $product->ship_cost }}</span>
                @else
                    <span class="text-success">
                        <strong>ENVIO SIN CARGO</strong>
                    </span>
                @endif
            </div>
            <div class="clearfix"></div>
            <button class="btn btn-danger detail-cart-size mt-3">
                <i class="fa fa-shopping-cart text-light"></i>
                Comprar
            </button>
        </div>   
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="category mt-md-5 mt-5">
                <h2 class="text-light text-center ml-3 mt-1 p-2">Descripción</h2>
            </div>
            <p class="mt-4 p-2 product-description text-justify">{{ $product->description }}</p>
        </div>
        <div class="col-md-6">
        </div>
    </div>
@endsection