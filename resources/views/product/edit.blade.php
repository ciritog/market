@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 mt-3">
                <h1 class="text-center">Editar producto</h1>
                <p class="text-center">Actualizá la información de tu producto.</p>
                @include('includes.message')
                <form method="POST" action="{{ route('product.update') }}" class="mt-5" enctype="multipart/form-data">
                    @csrf
    
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre del producto') }}</label>
    
                        <div class="col-md-6">
                            <input id="product_id" type="hidden" name="id" value="{{ $product->id }}">
                            <input id="name" type="text" placeholder='Ej: Notebook DELL 15"' class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $product->name }}">
    
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
    
                    <div class="form-group row">
                        <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Descripción') }}</label>
    
                        <div class="col-md-6">
                            <textarea name="description" class="form-control" id="description" placeholder="Describí tu producto">{{ $product->description }}</textarea>
    
                            @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
    
                    <div class="form-group row">
                        <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Precio') }}</label>
    
                        <div class="col-md-6">
                            <input id="price" type="number" class="form-control @error('price') is-invalid @enderror" name="price" min=0 value="{{ $product->price }}">
    
                            @error('price')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="stock" class="col-md-4 col-form-label text-md-right">{{ __('Stock') }}</label>
    
                        <div class="col-md-6">
                            <input id="stock" type="number" class="form-control @error('stock') is-invalid @enderror" name="stock" min=0 value="{{ $product->stock }}">
    
                            @error('stock')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="ship_cost" class="col-md-4 col-form-label text-md-right">{{ __('Costo de envio') }}</label>
    
                        <div class="col-md-6">
                            <input id="ship_cost" type="number" class="form-control @error('ship_cost') is-invalid @enderror" name="ship_cost" value="{{ $product->ship_cost }}">
    
                            @error('ship_cost')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mr-md-5 mb-2">
                        <div class="col-12 d-flex justify-content-center">
                            <img src="{{ route("product.image", ["filename" => $product->image]) }}" alt="{{ $product->name }}" width="100px" height="100px">
                        </div>
                    </div>

                    <div class="form-group row">
                        
                        <label for="image_path" class="col-md-4 col-form-label text-md-right">{{ __('Imagen del producto') }}</label>
    
                        <div class="col-md-6">
                        <input id="image_path" type="file" class="form-control @error('image_path') is-invalid @enderror" name="image_path">
    
                            @error('image_path')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
    
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Guardar cambios') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection