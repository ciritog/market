@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 mt-3">
                <h1 class="text-center">Agregar producto</h1>
                <p class="text-center">Agregá un producto para ofrecer en tu tienda.</p>
                <form method="POST" action="{{ route('product.save') }}" class="mt-5" enctype="multipart/form-data">
                    @csrf
    
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre del producto') }}</label>
    
                        <div class="col-md-6">
                            <input id="name" type="text" placeholder='Ej: Notebook DELL 15"' class="form-control @error('name') is-invalid @enderror" name="name" required>
    
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
    
                    <div class="form-group row">
                        <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Descripción') }}</label>
    
                        <div class="col-md-6">
                            <textarea name="description" class="form-control" id="description" placeholder="Describí tu producto" required></textarea>
    
                            @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
    
                    <div class="form-group row">
                        <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Precio') }}</label>
    
                        <div class="col-md-6">
                            <input id="price" type="number" class="form-control @error('price') is-invalid @enderror" name="price" min=0 required>
    
                            @error('price')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="stock" class="col-md-4 col-form-label text-md-right">{{ __('Stock') }}</label>
    
                        <div class="col-md-6">
                            <input id="stock" type="number" class="form-control @error('stock') is-invalid @enderror" name="stock" min=0 required>
    
                            @error('stock')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="ship_cost" class="col-md-4 col-form-label text-md-right">{{ __('Costo de envio') }}</label>
    
                        <div class="col-md-6">
                            <input id="ship_cost" type="number" class="form-control @error('ship_cost') is-invalid @enderror" name="ship_cost">
    
                            @error('ship_cost')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="category" class="col-md-4 col-form-label text-md-right">{{ __('Categoría') }}</label>
    
                        <div class="col-md-6">
                           
                            <select name="category" class="form-control">
                                @foreach ($categories as $category)
                                    <option class="option-font-size" value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
    
                            @error('category')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="image_path" class="col-md-4 col-form-label text-md-right">{{ __('Imagen del producto') }}</label>
    
                        <div class="col-md-6">
                            <input id="image_path" type="file" class="form-control @error('image_path') is-invalid @enderror" name="image_path" min=0>
    
                            @error('image_path')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
    
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Agregar') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection