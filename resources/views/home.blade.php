@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 mt-3">
            <h1>¡Hola, {{ Auth::user()->name }}!</h1>
            @if (Auth::user()->role == 'admin')
              <p class="h5">
                Este es el Panel de Administración de tu sitio web.
              </p>
            @else
                
            @endif
            <div class="card-deck mt-5">
                <div class="card">
                  <div class="card-body text-center">
                    <i class="fa fa-user-circle text-center account-icon-size"></i>
                    <h5 class="card-title mt-3">Mis datos</h5>
                    <p class="card-text">Los datos de tu cuenta</p>
                    <a href="{{ route("user.account") }}">
                      <button class="text-light btn bg-skyblue w-50">Ir</button>
                    </a>
                  </div>
                </div>
                @if (Auth::user()->role != 'admin')
                  <div class="card">
                    <div class="card-body text-center">
                      <i class="fa fa-envelope-open-o text-center account-icon-size"></i>
                      <h5 class="card-title mt-3">Mis pedidos</h5>
                      <p class="card-text">Los datos de tu cuenta</p>
                      <a href="#">
                        <button class="text-light btn bg-skyblue w-50">Ir</button>
                      </a>
                    </div>
                  </div>
                @else
                  <div class="card">
                    <div class="card-body text-center">
                      <i class="fa fa-envelope-open-o text-center account-icon-size"></i>
                      <h5 class="card-title mt-3">Pedidos</h5>
                      <p class="card-text">Los pedidos que te hicieron</p>
                      <a href="#">
                        <button class="text-light btn bg-skyblue w-50">Ir</button>
                      </a>
                    </div>
                  </div>
                @endif
                @if (Auth::user()->role == 'admin')
                  <div class="card">
                    <div class="card-body text-center">
                      <i class="fa fa-list text-center account-icon-size"></i>
                      <h5 class="card-title mt-3">Mis productos</h5>
                      <p class="card-text">Los productos de tu tienda</p>
                      <a href="{{ route("product.index") }}">
                        <button class="text-light btn bg-skyblue w-50">Ir</button>
                      </a>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-body text-center">
                      <i class="fa fa-list text-center account-icon-size"></i>
                      <h5 class="card-title mt-3">Categorías</h5>
                      <p class="card-text">Todas la categorías de tu tienda.</p>
                      <a href="{{ route("category.index") }}">
                        <button class="text-light btn bg-skyblue w-50">Ir</button>
                      </a>
                    </div>
                  </div>
                @endif
              </div>
        </div>
    </div>
</div>
@endsection
