USE market;

CREATE TABLE countries(
    id integer(255) AUTO_INCREMENT NOT NULL,
    name varchar(255) NOT NULL,
    created_at date NOT NULL,
    updated_at date NOT NULL,
    CONSTRAINT pk_countries PRIMARY KEY(id)
)ENGINE=InnoDb;

INSERT INTO countries VALUES(NULL, "Argentina", CURDATE(), CURDATE());

CREATE TABLE provinces(
    id integer(255) AUTO_INCREMENT NOT NULL,
    country_id integer(255) NOT NULL,
    name varchar(255) NOT NULL,
    created_at date NOT NULL,
    updated_at date NOT NULL,
    CONSTRAINT pk_provinces PRIMARY KEY(id),
    CONSTRAINT fk_provinces_country FOREIGN KEY(country_id) REFERENCES countries(id)
)ENGINE=InnoDb;

INSERT INTO provinces VALUES(NULL, 1, "Buenos Aires", CURDATE(), CURDATE());

CREATE TABLE cities(
    id integer(255) AUTO_INCREMENT NOT NULL,
    province_id integer(255) NOT NULL,
    name varchar(255) NOT NULL,
    created_at date NOT NULL,
    updated_at date NOT NULL,
    CONSTRAINT pk_cities PRIMARY KEY(id),
    CONSTRAINT fk_cities_provinces FOREIGN KEY(province_id) REFERENCES provinces(id)
)ENGINE=InnoDb;

INSERT INTO cities VALUES(NULL, 1, "Saladillo", CURDATE(), CURDATE());

CREATE TABLE orders(
    id integer(255) AUTO_INCREMENT NOT NULL,
    user_id int(255) NOT NULL,
    country_id integer(255) NOT NULL,
    province_id integer(255) NOT NULL,
    city_id integer(255) NOT NULL,
    address varchar(255) NOT NULL,
    cost    float(10,2) NOT NULL,
    status  varchar(255) NOT NULL,
    created_at date NOT NULL,
    updated_at date NOT NULL,
    CONSTRAINT pk_orders PRIMARY KEY(id),
    CONSTRAINT fk_orders_users FOREIGN KEY(user_id) REFERENCES users(id),
    CONSTRAINT fk_orders_country FOREIGN KEY(country_id) REFERENCES countries(id),
    CONSTRAINT fk_orders_province FOREIGN KEY(province_id) REFERENCES provinces(id),
    CONSTRAINT fk_orders_city FOREIGN KEY(city_id) REFERENCES cities(id)
)ENGINE=InnoDb;

CREATE TABLE categories(
    id integer(255) AUTO_INCREMENT NOT NULL,
    name varchar(255) NOT NULL,
    CONSTRAINT pk_categories PRIMARY KEY(id)
)ENGINE=InnoDb;

INSERT INTO orders VALUES(NULL, 1, 1, 1, 1, 'Av. Cabaral 2444', 1500.50, 'en proceso', CURDATE(), CURDATE());

CREATE TABLE products(
    id integer(255) AUTO_INCREMENT NOT NULL,
    category_id integer(255) NOT NULL,
    name varchar(255) NOT NULL,
    description text NOT NULL,
    price float(10,2) NOT NULL,
    ship_cost float(10,2) NOT NULL,
    created_at date NOT NULL,
    updated_at date NOT NULL,
    CONSTRAINT pk_products PRIMARY KEY(id),
    CONSTRAINT fk_products_category FOREIGN KEY(category_id) REFERENCES categories(id)
)ENGINE=InnoDb;

INSERT INTO orders VALUES(NULL, 'Silla Gamer', 'Una silla gamer muy cómoda', 20000.50, 500.50, CURDATE(), CURDATE());

CREATE TABLE images(
    id integer(255) AUTO_INCREMENT NOT NULL,
    product_id integer(255) NOT NULL,
    image_path varchar(255) NOT NULL,
    created_at date NOT NULL,
    updated_at date NOT NULL,
    CONSTRAINT pk_images PRIMARY KEY(id),
    CONSTRAINT fk_images_product FOREIGN KEY(product_id) REFERENCES products(id)
)ENGINE=InnoDb;

INSERT INTO images VALUES(NULL, 1, 'https://s3-sa-east-1.amazonaws.com/saasargentina/n6irXzOpzqijHmpe7Nrd/imagen', CURDATE(), CURDATE());

CREATE TABLE products_orders(
    id integer(255) AUTO_INCREMENT NOT NULL,
    order_id integer(255) NOT NULL,
    product_id integer(255) NOT NULL,
    units integer(255) NOT NULL,
    created_at date NOT NULL,
    updated_at date NOT NULL,
    CONSTRAINT pk_products_orders PRIMARY KEY(id),
    CONSTRAINT fk_products_orders_order FOREIGN KEY(order_id) REFERENCES orders(id),
    CONSTRAINT fk_products_orders_product FOREIGN KEY(product_id) REFERENCES products(id)
)ENGINE=InnoDb;

INSERT INTO products_orders VALUES(NULL, 2, 1, 10, CURDATE(), CURDATE());

CREATE TABLE users(
    id integer(255) AUTO_INCREMENT NOT NULL,
    name varchar(255) NOT NULL,
    surname varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    role varchar(255) NOT NULL,
    created_at date NOT NULL,
    updated_at date NOT NULL,
    remember_token varchar(255) NOT NULL
    CONSTRAINT pk_users PRIMARY KEY(id)
)ENGINE=InnoDb;

