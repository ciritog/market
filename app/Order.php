<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "orders";

    public function user(){
        return $this->hasOne('App\User');
    }

    // Many to one
    public function country(){
        return $this->belongsTo("App\Country");
    }

    // Many to one
    public function province(){
        return $this->belongsTo("App\Province");
    }

    // Many to one
    public function city(){
        return $this->belongsTo("App\City");
    }

}
