<?php

namespace App\Http\Controllers;

use App\Category;
use App\Image;
use App\Product;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ["except" => ["getImage", "detail"]]);
    }

    public function index(){
        $user = \Auth::user();

        if ($user->role == 'admin') {
            $products = Product::orderBy('id', 'desc')->paginate(5);

            return view('product.index', [
                "products" => $products
            ]);
        }else{
            return redirect()->route('home');
        }
    }

    public function add(){

        $categories = Category::all();

        return view('product.add', [
            "categories" => $categories
        ]);
    }


    public function edit($id){
        $product = Product::find($id);

        return view("product.edit", [
            "product" => $product
        ]);
    }

    public function save(Request $request){

        // Validate the form data
        $validate = $this->validate($request, [
            "name" => "required|string|max:255",
            "description" => "required|string",
            "price" => "required",
            "stock" => "required",
            "ship_cost" => "nullable",
            "category"  => "required|numeric",
            "image_path" => "required|image",
        ]);
        
        // Assign form data values to a variable
        $product_name = $request->input("name");
        $description = $request->input("description");
        $price = $request->input("price");
        $stock = $request->input("stock");
        $ship_cost = $request->input("ship_cost");
        $category = $request->input("category");
        $image = $request->file("image_path");

        // Store data in Product object
        $product = new Product();

        $product->name = $product_name;
        $product->description = $description;
        $product->price = $price;
        $product->stock = $stock;
        $product->ship_cost = $ship_cost;
        $product->category_id = $category;
        
        // Store image in DB and disk
        if($image){
            $image_path_name = time() . "_" . $image->getClientOriginalName();
            Storage::disk('products')->put($image_path_name, File::get($image));
            $product->image = $image_path_name;
        }
        
        $product->save();

        return redirect()->route('product.index')->with("message", "El producto fue agregado con éxito.");

    }

    public function update(Request $request){

        // Validate the form data
        $validate = $this->validate($request, [
            "id" => "required|numeric",
            "name" => "string|max:255",
            "description" => "string",
            "price" => "numeric",
            "stock" => "numeric",
            "ship_cost" => "numeric",
            "image_path" => "image",
        ]);

        // Assign form data values to a variable
        $product_id = $request->input("id");
        $product_name = $request->input("name");
        $description = $request->input("description");
        $price = $request->input("price");
        $stock = $request->input("stock");
        $ship_cost = $request->input("ship_cost");
        $image = $request->file('image_path');   
        

        // Find the right product by the id
        $product = Product::find($product_id);
        
        // Store data in object
        $product->name = $product_name;
        $product->description = $description;
        $product->price = $price;
        $product->stock = $stock;
        $product->ship_cost = $ship_cost;

        // Store image in DB and disk
        if($image){
            $image_path_name = time() . "_" . $image->getClientOriginalName();
            Storage::disk('products')->put($image_path_name, File::get($image));
            $product->image = $image_path_name;
        }

        $product->update();
                
        return redirect()->route('product.edit', [
                            "id" => $product->id
                          ])->with("message", "El producto fue actualizado correctamente.");
    }

    public function delete($id){

        $product = Product::find($id);

        $product->delete();

        return redirect()->route('product.index')->with("message", "El producto fue eliminado con éxito.");

    }

    public function detail($id){
        $product = Product::find($id);

        return view('product.detail', [
            'product' => $product
        ]);
    }

    public function getImage($filename){
        $file = Storage::disk('products')->get($filename);
        return $file;
    }

}
