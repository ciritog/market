<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ["except" => "products"]);
    }

    public function index(){
        $categories = Category::orderBy("id", "desc")->paginate(5);

        return view("category.index", [
            "categories" => $categories
        ]);
    }

    public function products($id){
        $category = Category::find($id);
        $category_name = $category->name;
        $products = $category->products;

        return view("category.products", [
            "category_name" => $category_name,
            "products" => $products
        ]);
    }

    public function add(){
        return view("category.add");
    }

    public function edit($id){

        $category = Category::find($id);

        return view("category.edit", [
            "category" => $category
        ]);

    }

    public function save(Request $request){
        $validate = $this->validate($request, [
            "name" => "required|string|max:255",
        ]);

        $name = $request->input("name");

        $category = new Category();
        $category->name = $name;
        $category->save();

        return redirect()->route("category.index")->with("message", "La categoría se agregó con éxito.");
    }

    public function update(Request $request){
        $validate = $this->validate($request, [
            "id" => "required|numeric",
            "name" => "string|max:255",
        ]);

        $id = $request->input("id");
        $name = $request->input("name");

        $category = Category::find($id);
        $category->name = $name;
        $category->update();

        return redirect()->route("category.edit", ["id" => $category->id])->with("message", "La categoría fue actualizada con éxito.");
    }

    public function delete($id){
        
        $category = Category::find($id);

        // Delete products of the category to delete
        $category->products()->delete();
        // Delete category
        $category->delete();

        return view("category.index")->with("message", "La categoría fue eliminada con éxito.");
    }
}
