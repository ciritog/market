<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function account(){
        return view('user.account');
    }

    public function update(Request $request){

        $user = Auth::user();
        $id = $user->id;

        // Validate information
        $validate = $this->validate($request, [
            "name" => "string|max:255",
            "surname" => "string|max:255",
            "email"   => "string|email|max:255|unique:users,email," . $id,
        ]);

        
        $name = $request->input("name");
        $surname = $request->input("surname");
        $email = $request->input("email");

        // Assign new values to the current User object
        $user->name = $name;
        $user->surname = $surname;
        $user->email = $email;

        // Update BD
        $user->update();

        return redirect()->route('user.account')->with('message', 'Tus datos fueron actualizados correctamente');

    }
}
