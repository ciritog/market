<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{

    public function index(){


        return view("cart.index");
    }

    public function add($product_id){

        // Product
        $product = Product::find($product_id);

        // My test cart array
        $cart = [
            "id" => $product->id,
            "product" => $product,
            "qty" => 1,
            "totalPrice" => $product->price
        ];

        if(session()->has('cart')){

            $items = session()->get('cart');
            $product_exists = false;

            foreach ($items as &$item) {
                if($item['id'] == $product->id){
                    $item['qty'] += 1;
                    $item['totalPrice'] += $product->price;
                    $product_exists = true;
                    session(["cart" => $items]);
                }
            }

            if(!$product_exists){
                session()->push('cart', $cart);
            }

            
        }else{
             // I create a session and add testCart to it.
            session(['cart' => array($cart)]);
        }

        // I return my view
        return view('cart.index');

    }

    public function delete($id = null){

        $cart = session()->get('cart');

        foreach ($cart as $index => $item) {
            if($id !== null){
                if ($item['id'] == $id) {
                    unset($cart[$index]);
                }
            }else{
                unset($cart[$index]);
            }
        }

        session(['cart' => $cart]);

        return redirect()->route('cart.index');
    }
}
