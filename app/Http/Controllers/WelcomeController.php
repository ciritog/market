<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Support\Facades\DB;

class WelcomeController extends Controller
{
    public function index(){

        $products = Product::orderBy('id', 'DESC')
                           ->limit(4)
                           ->get();

        $categories = Category::orderBy(DB::raw('RAND()'))
                                ->take(3)
                                ->get();
    
        return view('welcome', [
            "products" => $products,
            "categories" =>$categories
        ]);
    }
}
