<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOrder extends Model
{
    protected $table = "products_orders";

    // Many to one relationship
    public function order(){
        return $this->belongsTo("App\Order");
    }

    // Many to one relationship
    public function product(){
        return $this->belongsTo('App\Product');
    }
}
