<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = "countries";

    // One to many
    public function provinces(){
        return $this->hasMany("App\Province");
    }
}
