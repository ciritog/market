<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provinces';

    // One to One reletionship: One province has One country
    public function country(){
        return $this->hasOne('App\Country');
    }
}
