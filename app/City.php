<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = "cities";

    // One to One
    public function province(){
        return $this->hasOne("App\Province");
    }
}
