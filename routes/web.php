<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// GENERAL
Route::get('/', 'WelcomeController@index')->name('welcome');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

// USERS
Route::get('/misdatos', 'UserController@account')->name('user.account');
Route::post('/user/update', 'UserController@update')->name('user.update');

// CATEGORIES
Route::get('/categorias', 'CategoryController@index')->name("category.index");
Route::get('/categoria/{id}', 'CategoryController@products')->name("category.products");
Route::get('/categorias/agregar', 'CategoryController@add')->name("category.add");
Route::post('/categoria/save', 'CategoryController@save')->name("category.save");
Route::get('/categoria/{id}/editar', 'CategoryController@edit')->name("category.edit");
Route::post('/categoria/update', 'CategoryController@update')->name("category.update");
Route::get('/categoria/{id}/delete', 'CategoryController@delete')->name("category.delete");

// PRODUCTS
Route::get('/productos', 'ProductController@index')->name("product.index");
Route::get('/producto/agregar', 'ProductController@add')->name('product.add');
Route::post('/producto/save', 'ProductController@save')->name('product.save');
Route::get('/producto/editar/{id}', 'ProductController@edit')->name('product.edit');
Route::post('/producto/update', 'ProductController@update')->name('product.update');
Route::get('/producto/delete/{id}', 'ProductController@delete')->name('product.delete');
Route::get('/producto/{id}', 'ProductController@detail')->name('product.detail');
Route::get('/producto/image/{filename}', 'ProductController@getImage')->name('product.image');

// CART
Route::get('/carrito', 'CartController@index')->name("cart.index");
Route::get('/carrito/add/{id}', 'CartController@add')->name("cart.add");
Route::get('/carrito/delete/{id?}', 'CartController@delete')->name("cart.delete");

// ORDER
Route::get('/order', 'CartController@index')->name("cart.index");